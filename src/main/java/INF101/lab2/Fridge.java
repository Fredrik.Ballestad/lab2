package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    ArrayList<FridgeItem> innholdFridge = new ArrayList<FridgeItem>();

    private final int max_fridge = 20;

    @Override
    public int nItemsInFridge() {
        return innholdFridge.size();
    }

    @Override
    public int totalSize() {
        return max_fridge;
    }

    @Override
    public void emptyFridge() {
        innholdFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList();
        for(FridgeItem item : innholdFridge){
            if(item.hasExpired())
                expiredFood.add(item);
        }
        innholdFridge.removeAll(expiredFood);
        return expiredFood;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (innholdFridge.contains(item)) {
            innholdFridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (innholdFridge.size() < 20) {
            innholdFridge.add(item);
            return true;
        }
        return false;
    }
}
